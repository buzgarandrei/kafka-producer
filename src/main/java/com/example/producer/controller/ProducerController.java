package com.example.producer.controller;

import com.example.producer.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProducerController {

    private KafkaTemplate<String, User> kafkaTemplate;
    private String TOPIC = "topic";

    @Autowired
    public ProducerController(KafkaTemplate<String, User> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @PostMapping(value = "createUser")
    public String createUser(@RequestBody User user) {

        System.out.println("User produced successfully with name:"
                + user.getFirstName() + " " + user.getLastName());
        kafkaTemplate.send(TOPIC, user);
        return "User produced successfully";
    }
}
